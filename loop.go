package main

import (
	"fmt"
	"strings"
)

func main() {

	var firstName string
	var lastName string
	var email string
	var userTickets int
	var conferenceName string = "Go Conference"
	const conferenceTickets int = 50
	var remainingTickets uint = 50
	bookings := []string{}

	fmt.Printf("\n Welcome to %v\n if you want to perticipate this program then book a slot\n Total conference tickets = %v", conferenceName, conferenceTickets)

	for {

		fmt.Print("\n enter user first name: ")
		fmt.Scan(&firstName)

		fmt.Print("\n enter user last name: ")
		fmt.Scan(&lastName)

		fmt.Print("\n enter no of tickets that you want to book: ")
		fmt.Scan(&userTickets)

		fmt.Print("\n enter email id: ")
		fmt.Scan(&email)

		remainingTickets = remainingTickets - uint(userTickets)
		bookings = append(bookings, firstName+" "+lastName+",")
		fmt.Printf("\n booking for %v", bookings)
		fmt.Printf("\n Congratulations ! you %v %v have booked %v tickets, you will get confirmation email at %v", firstName, lastName, userTickets, email)
		fmt.Printf("\n Remaining Tickets %v", remainingTickets)

		//read first names
		firstNames := []string{}
		for _, booking := range bookings {
			var names = strings.Fields(booking)
			firstNames = append(firstNames, names[0])
		}
		fmt.Printf("\n First name :%v", firstNames)
	}
}
