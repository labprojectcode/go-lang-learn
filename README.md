`GO Programing Language Guide`

**Commands**

# Install Go 
https://go.dev/dl/  


# Create Module 
$ go mod init example/hello
go: creating new go.mod: module example/hello 


# Main Syntax
package main
import "fmt"

func main() {
      fmt.Println("Joker Said - 'Why So Serious !!'")
}


# Execute Code    
$ go run .
Hello, World!


# Need Help       
$ go help



**Links**

- Go Official Document : https://go.dev/doc/

- Go Tutorial : https://go.dev/learn/
 
- Go Package : https://go.dev/doc/tutorial/getting-started
 
- Go Playground : https://go.dev/play/
 
- Go Blogs : https://go.dev/blog/

